﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DemoDragNDrop.Classes;

namespace DemoDragNDrop
{
    public partial class Form1 : Form
    {
        List<Composant> Composants1;
        List<Composant> Composants2;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            initLists();
            refreshGroupBox(groupBox1, Composants1);
            groupBox1.AllowDrop = true;
            groupBox2.AllowDrop = true;
        }

        private void initLists()
        {
            Composants1 = new List<Composant>() {
                new Composant { Id = 1, Nom = "Composant1" },
                new Composant { Id = 2, Nom = "Composant2" },
                new Composant { Id = 3, Nom = "Composant3" },
            };
            Composants2 = new List<Composant>();
        }

        private void refreshGroupBox(GroupBox groupBox, List<Composant> composants)
        {
            const int padding_top = 18;
            const int padding_left = 1;
            const int margin = 3;
            groupBox.Controls.Clear();
            int count = 0;
            foreach(Composant composant in composants)
            {
                Label label = new Label
                {
                    Tag = composant,
                    Text = composant.Nom,
                };
                label.Location = new Point
                {
                    X = padding_left + margin,
                    Y = padding_top + count * (label.Height + 2 * margin)
                };
                label.MouseDown += Label_MouseDown;
                groupBox.Controls.Add(label);
                count++;
            }
        }

        private void Label_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                List<Composant> composants;
                Label label = (Label)sender;
                GroupBox origin = (GroupBox)label.Parent;
                origin.Tag = label;
                Composant composant = (Composant)label.Tag;
                DragDropEffects res = label.DoDragDrop(origin, DragDropEffects.Move);
                if (res == DragDropEffects.Move)
                {
                    composants = (origin.Name == "groupBox2") ? Composants2 : Composants1;
                    composants.Remove(composant);
                    refreshGroupBox(origin, composants);
                }
            }
        }

        private void GroupBox_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(GroupBox)))
            {
                GroupBox origin = (GroupBox)e.Data.GetData(typeof(GroupBox));
                if (origin != sender)
                {
                    e.Effect = DragDropEffects.Move;
                }
                else
                {
                    e.Effect = DragDropEffects.None;
                }
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void GroupBox_DragDrop(object sender, DragEventArgs e)
        {
            List<Composant> composants;
            GroupBox dest = (GroupBox)sender;
            GroupBox origin = (GroupBox)e.Data.GetData(typeof(GroupBox));
            composants = (dest.Name == "groupBox2") ? Composants2 : Composants1;
            Label label = (Label)origin.Tag;
            Composant composant = (Composant)label.Tag;
            composants.Add(composant);
            refreshGroupBox(dest, composants);
        }
    }
}
